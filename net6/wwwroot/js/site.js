﻿"use strict";
const help = () => {
    if (document.querySelector('div.tab-pane.fade.active.show') !== null) {
        introJs(`#${document.querySelector('div.tab-pane.fade.active.show').id}`).setOptions({
            'showProgress': false,
            'showBullets': false,
            'nextLabel': 'Siguiente',
            'prevLabel': 'Anterior',
            'skipLabel': '×',
            'doneLabel': 'Listo'
        }).start();
    } else {
        introJs().setOptions({
            'showProgress': false,
            'showBullets': false,
            'nextLabel': 'Siguiente',
            'prevLabel': 'Anterior',
            'skipLabel': '×',
            'doneLabel': 'Listo'
        }).start();
    }
};
