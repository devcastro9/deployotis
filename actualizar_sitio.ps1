﻿# Funcion de powershell
function Actualizar-Sitio {
    param (
        [Parameter()]
        [string]$Sitio,
        [Parameter()]
        [string]$RutaFuente,
        [Parameter()]
        [string]$RutaDestino,
        [Parameter()]
        [bool]$EsGit = $false
    )
    try {
        # Logica de funcion
        # Detenemos el sitio web
        Stop-IISSite -Name $Sitio -Confirm:$false
        # Actualizamos los archivos
        if ($EsGit) {
            [string]$old_ruta = Get-Location
            Set-Location -Path $RutaFuente
            git fetch
            git pull
            git gc
            Set-Location $old_ruta
        }
        # Borramos archivos anteriores
        Remove-Item -Recurse -Force "$($RutaDestino)\*"
        Robocopy $RutaFuente $RutaDestino *.* /s /mt
        # Iniciamos el sitio web
        Start-IISSite -Name $Sitio
    }
    catch {
        Write-Warning -Message 'Ha ocurrido un error al actualizar el sitio web.'
    }
}

# Solo modificar lineas de codigo hacia abajo
$nombre_sitio_web = "Clientes Web Site"
$ruta_proyecto = "C:\Users\Administrator\Documents\repos\deployotis\net6"
$ruta_sitio = "C:\inetpub\wwwroot"
# Solo modificar lineas de codigo hacia arriba

# Ejecucion del programa
Actualizar-Sitio -Sitio $nombre_sitio_web -RutaFuente $ruta_proyecto -RutaDestino $ruta_sitio -EsGit:$true